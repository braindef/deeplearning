#!/bin/bash

sudo apt-get install python-pip
sudo pip install cython
sudo pip install word2vec
sudo pip install gensim

sudo pip install cython --upgrade
sudo pip install word2vec --upgrade
sudo pip install gensim --upgrade
